(ns popsugar.core)

(defn rotate [ls] (concat (rest ls) [(first ls)]))

(defn rotate-n [ls n]
  (first (drop n (iterate rotate ls))))

(defn chairsleft [{chairs :chairs toskip :toskip}]
  {:chairs (rotate-n (drop 1 chairs) (first toskip)) :toskip (drop 1 toskip)})

(def eliminate 
  "Eliminate a person from the chairs according to the skip rules"
  (iterate chairsleft { :chairs (drop 1 (range 101)) :toskip (drop 1 (range))}))

(defn find-last-chair []
  ((first (drop 99 eliminate)) :chairs))

(defn -main [& args]
  (println "Last chair standing is" (find-last-chair)))
